#!/bin/bash
set -e
cd

exit_with() {
	echo "$@" >&2
	exit 1
}

if ! [ -d 'homepath' ]; then
	exit_with "${HOME}/homepath volume has not been found."
fi

check_file() {
	if ! [ -f "homepath/$1" ]; then
		exit_with "Necessary file $1 has not been found."
	fi
}

check_file etmain/pak0.pk3
check_file etmain/pak1.pk3
check_file etmain/pak2.pk3

if [ "$1" = 'start' ]; then
	exec ./etlded +set fs_homepath homepath +set fs_basepath basepath +set fs_game etbloat +exec etbloat
else
	exec "$@"
fi
