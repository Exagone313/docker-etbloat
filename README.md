# etbloat Docker image

This repository aims to ease the creation of an [ET: Legacy](https://www.etlegacy.com/) server
with [etbloat](https://gitlab.com/Exagone313/etbloat) mod.

Players can connect using 2.6-compatible clients (such as ET: Legacy) on different platforms:
* Windows (x86)
* GNU/Linux (i386, x86\_64, armhf)

macOS is not supported and there are no plans to support it, due to technical limitations.

This repository builds two services:
* etserver: game server
* webserver: an nginx server to download required paks (pk3)

## Installation

Images can be built with:

```
docker-compose build
```

Then create a `homepath` directory and copy paks (including maps) into an `etmain` subdirectory.

```
homepath
└── etmain
    ├── pak0.pk3
    ├── pak1.pk3
    └── pak2.pk3
```

Start the server using:

```
docker-compose up -d
```

This will start a local server.

## Configuration

Create an `etbloat` subdirectory in `homepath`, and then copy the original `etbloat.cfg` file
present in this repository.

Example directory structure:

```
homepath
├── etbloat
│   └── etbloat.cfg
└── etmain
    ├── caha_tavern_b2.pk3
    ├── canyon_depths_FINAL.pk3
    ├── mml_minastirith_fp3.pk3
    ├── pak0.pk3
    ├── pak1.pk3
    └── pak2.pk3
```

## Publish the server

To make the server publicly available, you can edit `docker-compose.override.yml`
to override published address and ports.

Don't forget to edit `sv_wwwBaseURL` cvar in a copied `etbloat.cfg` so that clients can download paks
on the running nginx server.

## License

### etbloat

Wolfenstein: Enemy Territory GPL Source Code
Copyright (C) 1999-2010 id Software LLC, a ZeniMax Media company.

OpenWolf GPL Source Code
Copyright (C) 2011 Dusan Jocic

ET: Legacy
Copyright (C) 2012-2020 ET:Legacy Team <mail@etlegacy.com>

etbloat
Copyright (C) 2016-2020 Elouan Martinet <exa@elou.world>

  etbloat is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  etbloat is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  etbloat (see COPYING.txt). If not, see <http://www.gnu.org/licenses/>.

  ADDITIONAL TERMS:  The Wolfenstein: Enemy Territory GPL Source Code is also
  subject to certain additional terms. You should have received a copy of these
  additional terms immediately following the terms and conditions of the GNU GPL
  which accompanied the Wolf ET Source Code.  If not, please request a copy in
  writing from id Software at id Software LLC, c/o ZeniMax Media Inc., Suite 120,
  Rockville, Maryland 20850 USA.

  EXCLUDED CODE:  The code described below and contained in the Wolfenstein:
  Enemy Territory GPL Source Code release is not part of the Program covered by
  the GPL and is expressly excluded from its terms.  You are solely responsible
  for obtaining from the copyright holder a license for such code and complying
  with the applicable license terms.


### MD4 Message-Digest Algorithm

Copyright (C) 1991-1992, RSA Data Security, Inc. Created 1991. All rights reserved.

  License to copy and use this software is granted provided that it is identified
  as the "RSA Data Security, Inc. MD4 Message-Digest Algorithm" in all mater
  ial mentioning or referencing this software or this function.

  License is also granted to make and use derivative works provided that such work
  s are identified as "derived from the RSA Data Security, Inc. MD4 Message-Digest
  Algorithm" in all material mentioning or referencing the derived work.

  RSA Data Security, Inc. makes no representations concerning either the merchanta
  bility of this software or the suitability of this software for any particular p
  urpose. It is provided "as is" without express or implied warranty of any
  kind.


### MD5 Message-Digest Algorithm

The MD5 algorithm was developed by Ron Rivest. The public domain C language
implementation used in this program was written by Colin Plumb in 1993, no copyright
is claimed.

  This software is in the public domain. Permission to use, copy, modify, and
  distribute this software and its documentation for any purpose and without fee is
  hereby granted, without any conditions or restrictions. This software is provided
  "as is" without express or implied warranty.
