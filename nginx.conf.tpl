worker_processes  $NGINX_PROCESSES;

error_log  /dev/stderr warn;
pid        /var/cache/nginx/nginx.pid;


events {
    worker_connections  $NGINX_CONNECTIONS;
}


http {
    types_hash_bucket_size 128;
    include                /etc/nginx/mime.types;
    default_type           application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /dev/stdout  main;

    sendfile        off;

    keepalive_timeout  65;

    gzip   off;

    server {
        listen       8080 default_server;
        server_name  _;
        location ~* \.pk3$ {
            root /srv/http;
            try_files /homepath$uri /basepath$uri =404;
        }
    }
}
