FROM debian:buster-slim AS build-common
RUN apt-get update && apt-get install -y --no-install-recommends \
	autoconf \
	bash \
	ca-certificates \
	cmake \
	git \
	make \
	nasm \
	zip \
	unzip \
	&& useradd -ms /bin/bash build
USER build
RUN mkdir -p ~/artifacts

FROM debian:buster-slim AS git-clone
RUN apt-get update && apt-get install -y --no-install-recommends \
	ca-certificates \
	git \
	&& useradd -ms /bin/bash build
USER build
ARG GIT_REPOSITORY
ARG GIT_VERSION
RUN cd \
	&& git clone $GIT_REPOSITORY source \
	&& cd source \
	&& git checkout $GIT_VERSION \
	&& git submodule update --init \
	&& git describe --long --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g' > ~/VERSION.txt

FROM build-common AS build-linux-x86-64
USER root
RUN apt-get install -y --no-install-recommends \
	g++-multilib \
	gcc-multilib
USER build
COPY --from=git-clone --chown=build:build /home/build/source /home/build/source
ARG MODNAME
ARG MAKEFLAGS
RUN cd ~/source \
	&& ./easybuild.sh build -64 -server -noextra \
	&& find ./build -name '*.pk3' -exec zip -d '{}' 'cgame.*' 'ui.*' ';' \
	&& mv build/etlded build/$MODNAME/*.pk3 build/$MODNAME/*.so ~/artifacts/

FROM build-common AS build-linux-i386
USER root
RUN apt-get install -y --no-install-recommends \
	g++-multilib \
	gcc-multilib
USER build
COPY --from=git-clone --chown=build:build /home/build/source /home/build/source
ARG MODNAME
ARG MAKEFLAGS
RUN cd ~/source \
	&& export BUILD_MOD_PK3=0 \
	&& ./easybuild.sh build -mod -noextra \
	&& mv build/$MODNAME/*.so ~/artifacts/

FROM build-common AS build-linux-armhf
USER root
RUN apt-get install -y --no-install-recommends \
	binutils-arm-linux-gnueabihf \
	g++-arm-linux-gnueabihf \
	gcc-arm-linux-gnueabihf \
	libc-dev-armhf-cross \
	&& ln -s /usr/bin/arm-linux-gnueabihf-gcc /usr/local/bin/gcc \
	&& ln -s /usr/bin/arm-linux-gnueabihf-g++ /usr/local/bin/g++
USER build
COPY --from=git-clone --chown=build:build /home/build/source /home/build/source
ARG MODNAME
ARG MAKEFLAGS
RUN cd ~/source \
	&& export BUILD_MOD_PK3=0 \
	&& export PATH="/usr/arm-linux-gnueabihf/bin:${PATH}" \
	&& export CC=arm-linux-gnueabihf-gcc \
	&& export CXX=arm-linux-gnueabihf-g++ \
	&& export CFLAGS="-I/usr/arm-linux-gnueabihf/include -L/usr/arm-linux-gnueabihf/lib" \
	&& export CXXFLAGS="${CFLAGS}" \
	&& ./easybuild.sh build -mod -noextra -RPI \
	&& mv build/$MODNAME/*.so ~/artifacts/ \
	&& cd ~/artifacts \
	&& for b in cgame qagame ui; do mv $b.mp.x86_64.so $b.mp.arm.so; done

FROM build-common AS build-windows-x86
USER root
RUN apt-get install -y --no-install-recommends \
	g++-mingw-w64-i686 \
	gcc-mingw-w64-i686
USER build
COPY --from=git-clone --chown=build:build /home/build/source /home/build/source
ARG MODNAME
ARG MAKEFLAGS
RUN cd ~/source \
	&& mkdir build && cd build \
	&& cmake .. -DCMAKE_TOOLCHAIN_FILE=../cmake/Toolchain-cross-mingw-linux.cmake \
	-DCMAKE_BUILD_TYPE=Release \
	-DCROSS_COMPILE32=1 \
	-DBUILD_SERVER=0 \
	-DBUILD_CLIENT=0 \
	-DBUILD_MOD_PK3=0 \
	-DINSTALL_EXTRA=0 \
	-DBUNDLED_SDL=0 \
	-DBUNDLED_JPEG=0 \
	-DBUNDLED_CURL=0 \
	-DBUNDLED_OPENSSL=0 \
	-DBUNDLED_OGG_VORBIS=0 \
	-DBUNDLED_THEORA=0 \
	-DBUNDLED_OPENAL=0 \
	-DBUNDLED_OPENAL_INCLUDE=0 \
	-DBUNDLED_GLEW=0 \
	-DBUNDLED_FREETYPE=0 \
	&& make \
	&& mv $MODNAME/*.dll ~/artifacts/

FROM build-common AS build-artifacts
COPY --from=git-clone /home/build/source/COPYING.txt /home/build/artifacts/
COPY --from=git-clone /home/build/VERSION.txt /home/build/artifacts/
COPY --from=build-linux-x86-64 /home/build/artifacts/* /home/build/artifacts/
COPY --from=build-linux-i386 /home/build/artifacts/* /home/build/artifacts/
COPY --from=build-linux-armhf /home/build/artifacts/* /home/build/artifacts/
COPY --from=build-windows-x86 /home/build/artifacts/* /home/build/artifacts/
ARG MODNAME
RUN cd ~/artifacts \
	&& mv etlded qagame.mp.x86_64.so .. \
	&& rm -f qagame.* \
	&& find . -name '*.pk3' -exec unzip '{}' ';' \
	&& rm -f *.pk3 \
	&& zip -0 -r "../${MODNAME}_$(cat VERSION.txt).pk3" * \
	&& cd .. \
	&& rm -rf artifacts \
	&& mkdir artifacts \
	&& mv etlded qagame.mp.x86_64.so *.pk3 artifacts

FROM nginx:stable AS webserver
RUN mkdir -p /srv/http/homepath /srv/http/basepath/$MODNAME
ARG NGINX_PROCESSES=1
ARG NGINX_CONNECTIONS=64
COPY ./nginx.conf.tpl /tmp/
RUN envsubst < /tmp/nginx.conf.tpl '$NGINX_PROCESSES$NGINX_CONNECTIONS' > /etc/nginx/nginx.conf \
	&& rm /tmp/nginx.conf.tpl
RUN chown nginx:nginx /var/cache/nginx
USER nginx
# gracefully shut down nginx
STOPSIGNAL SIGQUIT
ARG MODNAME
COPY --from=build-artifacts /home/build/artifacts/*.pk3 /srv/http/basepath/$MODNAME/

FROM debian:buster-slim AS etserver
ARG UID=1000
ARG GID=1000
COPY ./entrypoint.sh /
RUN groupadd -g $GID -o server \
	&& useradd -u $UID -g $GID -ms /bin/bash server
USER server
WORKDIR /home/server
ARG MODNAME
RUN mkdir -p basepath/$MODNAME
COPY --from=build-artifacts /home/build/artifacts/etlded /home/server/
COPY --from=build-artifacts /home/build/artifacts/qagame.mp.x86_64.so /home/build/artifacts/*.pk3 /home/server/basepath/$MODNAME/
COPY ./etbloat.cfg /home/server/basepath/$MODNAME/
ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
CMD ["start"]
